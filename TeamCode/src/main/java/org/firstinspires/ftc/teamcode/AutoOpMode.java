package org.firstinspires.ftc.teamcode;

import android.graphics.Color;
import android.text.method.Touch;

import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.TouchSensor;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

import com.qualcomm.robotcore.util.ElapsedTime;

import java.util.Locale;

public class AutoOpMode
{
    private Telemetry telemetry;
    private DcMotor frontLeftDrive;
    private DcMotor frontRightDrive;
    private DcMotor backLeftDrive;
    private DcMotor backRightDrive;
    private DcMotor ticksEncoder;
    private ColorSensor sensorColor;
    private DistanceSensor sensorDistance;
    private DistanceSensor robotDetector;
    private TouchSensor sideSensorToLoading;
    private TouchSensor sideSensorToBuilding;
    private TouchSensor backSensor;

    private Pince pince;
    private ElapsedTime runtime;

    private double lastTicks;
    private float hsvValues[] = {0F, 0F, 0F};
    private final double SCALE_FACTOR = 255;
    private boolean isBlue;
    private boolean isMovingFoundation;

    // Define variables that will directly affect robot
    private double ticksPerMeterX = 3300;
    private double ticksPerMeterY = 4000;

    private double generalSpeed = 1.0;
    private double slowSpeed = 0.4;

    private double maximumRuntime = 29.8;

    private boolean checkActive;

    AutoOpMode(
            boolean isOnBlueAlliance,
            Telemetry globalTelemetry,
            Pince globalPince,
            DcMotor FL, DcMotor FR, DcMotor BL, DcMotor BR,
            ColorSensor color, DistanceSensor distance,
            TouchSensor sideLeft,
            TouchSensor sideRight,
            TouchSensor back,
            DistanceSensor robot,
            ElapsedTime globalRuntime
            ) {
        telemetry = globalTelemetry;
        frontLeftDrive = FL;
        frontRightDrive = FR;
        backLeftDrive = BL;
        backRightDrive = BR;
        sensorColor = color;
        sensorDistance = distance;
        robotDetector = robot;
        runtime = globalRuntime;
        if (!isBlue) { // TODO: Check
            sideSensorToBuilding = sideLeft;
            sideSensorToLoading = sideRight;
        } else {
            sideSensorToBuilding = sideRight;
            sideSensorToLoading = sideLeft;
        }
        backSensor = back;
        pince = globalPince;
        isBlue = isOnBlueAlliance;
    }

    public void moveLeft(double distanceInMeters, double speed) {
        double tickDistance = distanceInMeters * ticksPerMeterX;
        // If the alliance is blue or the robot is pulling the foundation then use normal controls
        if (isBlue || isMovingFoundation) {
            while (ticksEncoder.getCurrentPosition() < lastTicks + tickDistance && (runtime.time() < maximumRuntime)) {
                frontLeftDrive.setPower(speed);
                frontRightDrive.setPower(speed);
                backLeftDrive.setPower(speed);
                backRightDrive.setPower(speed);
            }
        } else {
            while (ticksEncoder.getCurrentPosition() > lastTicks - tickDistance && (runtime.time() < maximumRuntime)) {
                frontLeftDrive.setPower(-speed);
                frontRightDrive.setPower(-speed);
                backLeftDrive.setPower(-speed);
                backRightDrive.setPower(-speed);
            }
        }
        lastTicks = ticksEncoder.getCurrentPosition();
    }

    public void moveRight(double distanceInMeters, double speed) {
        double tickDistance = distanceInMeters * ticksPerMeterX;
        // If the alliance is blue or the robot is pulling the foundation then use normal controls
        if (isBlue || isMovingFoundation) {
            while (ticksEncoder.getCurrentPosition() > lastTicks - tickDistance && (runtime.time() < maximumRuntime)) {
                frontLeftDrive.setPower(-speed);
                frontRightDrive.setPower(-speed);
                backLeftDrive.setPower(-speed);
                backRightDrive.setPower(-speed);
            }
        } else {
            while (ticksEncoder.getCurrentPosition() < lastTicks + tickDistance && (runtime.time() < maximumRuntime)) {
                frontLeftDrive.setPower(speed);
                frontRightDrive.setPower(speed);
                backLeftDrive.setPower(speed);
                backRightDrive.setPower(speed);
            }
        }
        lastTicks = ticksEncoder.getCurrentPosition();
    }

    public void moveForward(double distanceInMeters, double speed) {
        double tickDistance = distanceInMeters * ticksPerMeterY;
        // If on red team and moving foundation, then switch up and down
        if (!isBlue && isMovingFoundation) {
            while (ticksEncoder.getCurrentPosition() < lastTicks + tickDistance && (runtime.time() < maximumRuntime)) {
                frontLeftDrive.setPower(speed);
                frontRightDrive.setPower(-speed);
                backLeftDrive.setPower(-speed);
                backRightDrive.setPower(speed);
            }
        } else {
            while (ticksEncoder.getCurrentPosition() > lastTicks - tickDistance && (runtime.time() < maximumRuntime)) {
                frontLeftDrive.setPower(-speed);
                frontRightDrive.setPower(speed);
                backLeftDrive.setPower(speed);
                backRightDrive.setPower(-speed);
            }
        }
        lastTicks = ticksEncoder.getCurrentPosition();
    }

    public void moveBackward(double distanceInMeters, double speed) {
        double tickDistance = distanceInMeters * ticksPerMeterY;
        // If on red team and moving foundation, then switch up and down
        if (!isBlue && isMovingFoundation) {
            while (ticksEncoder.getCurrentPosition() > lastTicks - tickDistance && (runtime.time() < maximumRuntime)) {
                frontLeftDrive.setPower(-speed);
                frontRightDrive.setPower(speed);
                backLeftDrive.setPower(speed);
                backRightDrive.setPower(-speed);
            }
        } else {
            while (ticksEncoder.getCurrentPosition() < lastTicks + tickDistance && (runtime.time() < maximumRuntime)) {
                frontLeftDrive.setPower(speed);
                frontRightDrive.setPower(-speed);
                backLeftDrive.setPower(-speed);
                backRightDrive.setPower(speed);
            }
        }
        lastTicks = ticksEncoder.getCurrentPosition();
    }

    public void stopMoving() {
        frontLeftDrive.setPower(0);
        frontRightDrive.setPower(0);
        backLeftDrive.setPower(0);
        backRightDrive.setPower(0);
        lastTicks = ticksEncoder.getCurrentPosition();
    }

    public void scanColor() {
        Color.RGBToHSV(
                (int) (sensorColor.red() * SCALE_FACTOR),
                (int) (sensorColor.green() * SCALE_FACTOR),
                (int) (sensorColor.blue() * SCALE_FACTOR),
                hsvValues
        );
    }

    public void stayStill(double seconds) {
        stopMoving();
        delay(seconds);
    }

    public void delay(double seconds) {
        double startTime = runtime.time();
        while ((startTime + seconds) > runtime.time() && (runtime.time() < maximumRuntime));
    }

    public void log(String caption, String value) {
        telemetry.addData(caption, value);
        telemetry.update();
    }

    public void initFoundationOpMode() {
        frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive.setDirection(DcMotor.Direction.FORWARD);
        backLeftDrive.setDirection(DcMotor.Direction.REVERSE);
        backRightDrive.setDirection(DcMotor.Direction.REVERSE);
        // If you change this motor, you also have to change:
        //  - the < / > or - / + symbols in moveLeft, moveRight, moveForward and moveBackwards
        //  - the ticksPerMeterX and ticksPerMeterY to match this new motor
        isMovingFoundation = true;

        ticksEncoder = backRightDrive;

        pince.open();
        pince.openFoundation();
    }

    public void runFoundationOpMode() {
        lastTicks = ticksEncoder.getCurrentPosition();

        log("Action:", "Moving to foundation");
        moveBackward(0.5, generalSpeed);
        moveLeft(0.7, generalSpeed);
        moveLeft(0.2, slowSpeed);

        log("Action:", "Grabbing foundation");
        stayStill(0.25);
        pince.closeFoundation();
        stayStill(0.25);

        log("Action:", "Going back to building corner");
        double start = runtime.time();
        while(!sideSensorToLoading.isPressed()) {
            moveRight(0.02, generalSpeed);
            if (runtime.time() - start > 6 || (runtime.time() > maximumRuntime)) break;
        }
        /*
        moveRight(0.1, slowSpeed);
        stayStill(0.25);
        moveForward(0.25, slowSpeed);
        moveRight(0.15, slowSpeed);
         */

        log("Action:", "Dropping foundation");
        pince.openFoundation();
        stayStill(0.25);

        log("Action:", "Pushing Foundation");
        moveForward(0.83, generalSpeed);
        stayStill(0.1);
        moveLeft(0.3, generalSpeed);
        stayStill(0.1);
        moveBackward(0.5, generalSpeed); // CAN OPTIMIZE HERE

        log("Action:", "Going under Bridge");

        /*moveForward(0.7, generalSpeed); // TODO: Check touch sensor
        moveForward(0.1, slowSpeed);*/

        if (isBlue) {
            double sideDistance = 0.0;
            double forwardDistance = 0.3;
            boolean lastSensor = false;
            while (sideDistance < 0.8 && (runtime.time() < 29.8)) {
                telemetry.addData("Action:", "Avoiding robots");
                telemetry.addData("Distance", String.format(Locale.US, "%.02f", sensorDistance.getDistance(DistanceUnit.CM)));
                telemetry.update();
                if (sensorDistance.getDistance(DistanceUnit.CM) < 30 && !Double.isNaN(sensorDistance.getDistance(DistanceUnit.CM))) {
                    telemetry.addData("Distance < 30 and not NaN", "");
                    telemetry.update();
                    //moveForward(0.1, slowSpeed);
                    moveLeft(0.03, generalSpeed);
                    forwardDistance += 0.03;
                    lastSensor = true;
                } else {
                    if (lastSensor) {
                        telemetry.addData("Lastsensor", "");
                        telemetry.update();
                        stopMoving();
                        //moveForward(0.2, slowSpeed);
                        moveLeft(0.1, generalSpeed);
                        forwardDistance += 0.1;
                        stopMoving();

                    } else {
                        telemetry.addData("MoveLeft", "");
                        telemetry.update();
                        //moveLeft(0.1, slowSpeed);
                        moveForward(0.03, generalSpeed);
                        sideDistance += 0.03;
                    }
                    lastSensor = false;
                }
                if (forwardDistance > 0.3) {
                    //moveBackward(0.8, slowSpeed);
                    double start2 = runtime.time();
                    while (!backSensor.isPressed()) {
                        moveRight(0.02, generalSpeed);
                        if (runtime.time() - start2 > 5 || (runtime.time() > maximumRuntime)) break;
                    }
                    moveRight(0.02, slowSpeed);
                    forwardDistance = 0;
                }
            }
        } else {
            moveForward(0.8, generalSpeed);
        }

        stopMoving();

        /*double forwardDistance = 0.0;
        double sideDistance = 0.0;
        boolean lastSensor = false;
        while (forwardDistance < 0.4 && (runtime.time() < maximumRuntime))  {
            telemetry.addData("Action:", "Avoiding robots");
            telemetry.addData("Distance", String.format(Locale.US, "%.02f", robotDetector.getDistance(DistanceUnit.CM)));
            telemetry.update();
            if (robotDetector.getDistance(DistanceUnit.CM) < 30 && !Double.isNaN(robotDetector.getDistance(DistanceUnit.CM))) {
                moveLeft(0.1, slowSpeed);
                sideDistance += 0.1;
                lastSensor = true;
            } else {
                if (lastSensor) {
                    moveLeft(0.2, slowSpeed);
                    sideDistance += 0.2;
                } else {
                    moveForward(0.1, slowSpeed);
                    forwardDistance += 0.1;
                }
                lastSensor = false;
            }
            if (sideDistance > 0.6) {
                moveRight(0.8, slowSpeed);
                sideDistance -= 0.6;
            }
        }
        moveForward(0.4, generalSpeed);
         */
        stopMoving();

        log("Action:", "Finished");
    }

    public void initSingleSkyStoneOpMode() {
        isMovingFoundation = false;
        frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive.setDirection(DcMotor.Direction.FORWARD);
        backLeftDrive.setDirection(DcMotor.Direction.REVERSE);
        backRightDrive.setDirection(DcMotor.Direction.REVERSE);

        // If you change this motor, you also have to change:
        //  - the < / > or - / + symbols in moveLeft, moveRight, moveForward and moveBackwards
        //  - the ticksPerMeterX and ticksPerMeterY to match this new motor
        ticksEncoder = backRightDrive;

        pince.open();
    }

    public void runSingleSkyStoneOpMode() {
        lastTicks = ticksEncoder.getCurrentPosition();

        pince.turnForwards();
        pince.open();

        int scanCount = 0;

        do {

            scanCount = 0;

            pince.open();

            log("Action:", "Moving to brick position");
            double start = runtime.time();
            while (!sideSensorToLoading.isPressed() && (runtime.time() < maximumRuntime)) {
                moveRight(0.02, generalSpeed);
                if (runtime.time() - start > 5) break;
            }
            moveRight(0.05, slowSpeed);
            //stayStill(0.1);
            stopMoving();

            double startTime = runtime.time();


            /*for (int i = 0; i < 2; i++) { //while (sensorDistance.getDistance(DistanceUnit.CM) > 30 || Double.isNaN(sensorDistance.getDistance(DistanceUnit.CM))) {
                moveForward(0.22, generalSpeed);
                if (!sideSensorToLoading.isPressed()) {
                    moveRight(0.05, generalSpeed);
                }
                if (runtime.time() - startTime > 4 || (runtime.time() > maximumRuntime)) {
                    break;
                }
            }*/
            moveForward(0.44, generalSpeed);

            startTime = runtime.time();
            while (sensorDistance.getDistance(DistanceUnit.CM) > 6 || Double.isNaN(sensorDistance.getDistance(DistanceUnit.CM))) {
                pince.turnForwards();
                telemetry.addData("Action:", "Detecting Bricks");
                telemetry.addData("Distance", String.format(Locale.US, "%.02f", sensorDistance.getDistance(DistanceUnit.CM)));
                telemetry.update();
                moveForward(0.02, slowSpeed);
                if (!sideSensorToLoading.isPressed()) {
                    moveRight(0.05, slowSpeed);
                }
                //moveForward(0.02, generalSpeed);
                //moveRight(0.01, generalSpeed);
                if (runtime.time() - startTime > 6 || (runtime.time() > maximumRuntime)) {
                //if (runtime.time() - startTime > 2) {
                    break;
                }
            }

            telemetry.addData("Action:", "Found line of bricks");
            telemetry.addData("Distance", String.format(Locale.US, "%.02f", sensorDistance.getDistance(DistanceUnit.CM)));
            telemetry.update();
            //stayStill(0.2);
            stopMoving();
            scanColor();
            while (hsvValues[2] < 80 || hsvValues[2] > 1400 || hsvValues[1] > 0.5) { // Left this at slowSpeed
                pince.turnForwards();
                scanColor();
                moveLeft(0.03, slowSpeed);
                stopMoving();
                telemetry.addData("Action:", "Searching for skystone");
                telemetry.addData("Red", sensorColor.red());
                telemetry.addData("Green", sensorColor.green());
                telemetry.addData("Blue", sensorColor.blue());
                telemetry.addData("Alpha", sensorColor.alpha());
                telemetry.addData("Scancount ", scanCount);

                telemetry.update();

                /*if (sensorDistance.getDistance(DistanceUnit.CM) > 7 && sensorDistance.getDistance(DistanceUnit.CM) < 15 && !Double.isNaN(sensorDistance.getDistance(DistanceUnit.CM))) {
                    log("Action: ", "TOO FAR");
                    delay(0.3);
                    moveForward(0.01, slowSpeed);
                    stopMoving();
                }*/


                scanCount++;
                if (scanCount > 30 || (runtime.time() > maximumRuntime)) { // Abort if robot can't find skystone.
                    break;
                }
            }

            log("Action: ", "Found Skystone or last brick");
            //stayStill(0.1);
            moveForward(0.01, slowSpeed);
            //stayStill(0.1);
            pince.close();
            stayStill(0.2);

            log("Action:", "Heading to drop off position");

            start = runtime.time();
            while(!backSensor.isPressed()) {
                moveBackward(0.02, generalSpeed);
                if (runtime.time() - start > 5 || (runtime.time() > maximumRuntime)) break;
            }
            //stayStill(0.1);
            moveForward(0.02, generalSpeed);

        } while ((sensorDistance.getDistance(DistanceUnit.CM) > 7 || Double.isNaN(sensorDistance.getDistance(DistanceUnit.CM))) && runtime.time() < 26);

        //moveForward(0.01, slowSpeed);
        moveForward(0.01, generalSpeed);

        moveLeft(0.8 - scanCount * 0.05, generalSpeed);

        stopMoving();

        /*double start = runtime.time();
        while(!backSensor.isPressed()) {
            moveBackward(0.01, generalSpeed);
            if (runtime.time() - start > 5 || (runtime.time() > maximumRuntime)) break;
        }
        moveBackward(0.2, slowSpeed);*/

        log("Action:", "Moving to position under bridge");

        double sideDistance = 0.0;
        double forwardDistance = 0.0;
        boolean lastSensor = false;
        while (sideDistance < 0.8 && (runtime.time() < 29.8)) {
            telemetry.addData("Action:", "Avoiding robots");
            telemetry.addData("Distance", String.format(Locale.US, "%.02f", robotDetector.getDistance(DistanceUnit.CM)));
            telemetry.update();
            if (robotDetector.getDistance(DistanceUnit.CM) < 30 && !Double.isNaN(robotDetector.getDistance(DistanceUnit.CM)) ) {
                telemetry.addData("Distance < 30 and not NaN", "");
                telemetry.update();
                //moveForward(0.1, slowSpeed);
                moveForward(0.03, generalSpeed);
                forwardDistance += 0.03;
                lastSensor = true;
            } else {
                if (lastSensor) {
                    telemetry.addData("Lastsensor", "");
                    telemetry.update();
                    stopMoving();
                    //moveForward(0.2, slowSpeed);
                    moveForward(0.2, generalSpeed);
                    forwardDistance += 0.1;
                    stopMoving();
                    if (forwardDistance > 0.3) {
                        stopMoving();
                        if (isBlue) {
                            frontRightDrive.setPower(0.3);
                        } else {
                            frontRightDrive.setPower(-0.3);
                        }
                        double start3 = runtime.time();
                        while(runtime.time() - start3 < 0.2 && (runtime.time() < maximumRuntime)) {
                        }
                    }
                    stopMoving();

                } else {
                    telemetry.addData("MoveLeft", "");
                    telemetry.update();
                    //moveLeft(0.1, slowSpeed);
                    moveLeft(0.03, generalSpeed);
                    sideDistance += 0.03;
                }
                lastSensor = false;
            }
            if (forwardDistance > 0.4) {
                //moveBackward(0.8, slowSpeed);
                double start2 = runtime.time();
                while(!backSensor.isPressed()) {
                    moveBackward(0.02, generalSpeed);
                    if (runtime.time() - start2 > 5 || (runtime.time() > maximumRuntime)) break;
                }
                moveBackward(0.02, slowSpeed);
                forwardDistance = 0;
            }
            if (sideSensorToBuilding.isPressed()) {
                break;
            }
        }

        pince.open();
        stayStill(0.2);

        //moveBackward(forwardDistance, slowSpeed);
        //moveBackward(forwardDistance, generalSpeed);

        pince.turnBackwards();

        for (int i = 0; i<5; i++) {
            moveRight(0.047, generalSpeed);
            if (sideSensorToLoading.isPressed()) {
                break;
            }
        }

        log("Action:", "Finished");
        stopMoving();
    }

    void goBackAfterSkystone() {
        moveRight(0.4, generalSpeed);
        double start = runtime.time();
        while(!backSensor.isPressed()) {
            moveBackward(0.02, generalSpeed);
            if (runtime.time() - start > 5 || (runtime.time() > maximumRuntime)) break;
        }
        stopMoving();
    }

    void turnForSkystone() {
    moveForward(0.9, generalSpeed); // HERE
        double start = runtime.time();
        while(!sideSensorToLoading.isPressed()) {
            if (isBlue) {
                moveRight(0.02, generalSpeed);
            } else {
                moveLeft(0.02, generalSpeed);
            }
            if (runtime.time() - start > 5 || (runtime.time() > maximumRuntime)) break;
        }
        stayStill(0.1);
        start = runtime.time();
        while(!backSensor.isPressed()) {
            if (isBlue) {
                frontLeftDrive.setPower(1);
                backLeftDrive.setPower(-1);
            } else {
                frontLeftDrive.setPower(1);
                backLeftDrive.setPower(-1);
            }

            if (runtime.time() - start > 2.5 || (runtime.time() > maximumRuntime)) break;
        }
    }
}


/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name="Blue (left) | foundation and skystone (building)", group="Linear Opmode")
public class AutoOpModeBlueFoundationSkystone extends LinearOpMode {
    private Pince pince;
    private AutoOpMode auto;

    private ElapsedTime runtime = new ElapsedTime();


    @Override
    public void runOpMode() {
        pince = new Pince(
                telemetry,
                hardwareMap.get(DcMotor.class, "lift_motor1"),
                hardwareMap.get(DcMotor.class, "lift_motor2"),
                hardwareMap.get(CRServo.class, "arm_servo"),
                hardwareMap.get(Servo.class, "rotation_servo"),
                hardwareMap.get(Servo.class, "grip_servo"),
                hardwareMap.get(Servo.class, "foundation_servo1"),
                hardwareMap.get(Servo.class, "foundation_servo2")
        );
        auto = new AutoOpMode(
                true,
                telemetry,
                pince,
                hardwareMap.get(DcMotor.class, "front_left_drive"),
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive"),
                hardwareMap.get(ColorSensor.class, "sensor_color"),
                hardwareMap.get(DistanceSensor.class, "sensor_color"),
                hardwareMap.get(TouchSensor.class, "side_sensor_left"),
                hardwareMap.get(TouchSensor.class, "side_sensor_right"),
                hardwareMap.get(TouchSensor.class, "back_sensor"),
                hardwareMap.get(DistanceSensor.class, "robot_detector"),
                runtime
        );
        auto.initFoundationOpMode();
        waitForStart();
        runtime.reset();
        auto.runFoundationOpMode();
        if (runtime.time() < 14) {
            auto.initSingleSkyStoneOpMode(); // CHECK
            auto.turnForSkystone();
            telemetry.addData("New skystone at ", runtime.time());
            telemetry.update();
            auto.runSingleSkyStoneOpMode();
        } else {
            telemetry.addData("Finished at ", runtime.time());
            telemetry.update();
        }
    }
}

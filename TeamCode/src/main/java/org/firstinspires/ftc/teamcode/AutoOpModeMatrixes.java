package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.matrices.GeneralMatrixF;
import org.firstinspires.ftc.robotcore.external.matrices.MatrixF;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;

@TeleOp(name="Autonomous Iterative OpMode Matrices")
public class AutoOpModeMatrixes extends OpMode
{
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////

    private Pince pince;

    // ROBOT PARAMETERS
    private float a = 0.13f;
    private float b = 0.14f;
    private float r = 0.04f;

    private float couplingFactor = 0.5f;

    // POSITION VECTORS
    private VectorF wheelSenseCoef;
    private VectorF ticksPerRevolution;

    private VectorF wheelSensePos;
    private VectorF wheelSenseDelta;
    private VectorF wheelSenseDeltaNorm;
    private VectorF wheelScaleFactor;
    private VectorF wheelSensePrevPos;
    private VectorF robotSenseDelta;
    private VectorF worldSenseDelta;

    private VectorF worldPosition;
    private VectorF worldTarget;

    private VectorF worldPlanDelta;
    private VectorF robotPlanDelta;
    private VectorF wheelPlanDelta;
    private VectorF wheelPlanDeltaNorm;
    private VectorF wheelPower;
    private VectorF wheelPowerNorm;

    // TRANSFORMATION MATRICES
    private MatrixF wheelToRobot;
    private MatrixF robotToWorld;
    private MatrixF worldToRobot;
    private MatrixF robotToWheel;

    private float cosHeading = 1.0f;
    private float sinHeading = 0.0f;

    private float deviation = 0.0f;

    private DcMotor frontLeftDrive;
    private DcMotor frontRightDrive;
    private DcMotor backLeftDrive;
    private DcMotor backRightDrive;

    ///////////////////////////////////////////////////////// INIT METHODS /////////////////////////////////////////////////////////

    @Override
    public void init() {
        frontLeftDrive  = hardwareMap.get(DcMotor.class, "front_left_drive");
        frontRightDrive = hardwareMap.get(DcMotor.class, "front_right_drive");
        backLeftDrive   = hardwareMap.get(DcMotor.class, "back_left_drive");
        backRightDrive  = hardwareMap.get(DcMotor.class, "back_right_drive");

        pince = new Pince(
                telemetry,
                hardwareMap.get(DcMotor.class, "lift_motor1"),
                hardwareMap.get(DcMotor.class, "lift_motor2"),
                hardwareMap.get(CRServo.class, "arm_servo"),
                hardwareMap.get(Servo.class, "rotation_servo"),
                hardwareMap.get(Servo.class, "grip_servo"),
                hardwareMap.get(Servo.class, "foundation_servo1"),
                hardwareMap.get(Servo.class, "foundation_servo2")

        );

        // OBJECTS CREATION
        wheelToRobot = new GeneralMatrixF(4, 4);
        robotToWorld = new GeneralMatrixF(4, 4);
        worldToRobot = new GeneralMatrixF(4, 4);
        robotToWheel = new GeneralMatrixF(4, 4);

        // ticksPerRevolution = new VectorF(581.9f, 529.0f, 528.5f, 528.8f);
        wheelSenseCoef = new VectorF(0.012108663f, 0.011877476f, 0.011888713f, 0.011881969f);

        wheelSensePos       = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        wheelSenseDelta     = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        wheelSenseDeltaNorm = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        wheelScaleFactor    = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        wheelSensePrevPos   = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        robotSenseDelta     = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        worldSenseDelta     = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);

        worldPosition       = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        worldTarget         = new VectorF(10.0f, 0.0f, 0.0f, 0.0f);

        worldPlanDelta      = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        robotPlanDelta      = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        wheelPlanDelta      = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        wheelPlanDeltaNorm  = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        wheelPower          = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);
        wheelPowerNorm      = new VectorF(0.0f, 0.0f, 0.0f, 0.0f);

        // WHEEL TO ROBOT TRANSFORMATION MATRIX INITIALIZATION
        float wtrf = 1.0f / (a + b);

        wheelToRobot.put(0, 0,  1.0f); wheelToRobot.put(0, 1,  1.0f); wheelToRobot.put(0, 2,  1.0f); wheelToRobot.put(0, 3,  1.0f);
        wheelToRobot.put(1, 0,  1.0f); wheelToRobot.put(1, 1, -1.0f); wheelToRobot.put(1, 2,  1.0f); wheelToRobot.put(1, 3, -1.0f);
        wheelToRobot.put(2, 0,  wtrf); wheelToRobot.put(2, 1, -wtrf); wheelToRobot.put(2, 2, -wtrf); wheelToRobot.put(2, 3,  wtrf);
        wheelToRobot.put(3, 0,  1.0f); wheelToRobot.put(3, 1,  1.0f); wheelToRobot.put(3, 2, -1.0f); wheelToRobot.put(3, 3, -1.0f);

        wheelToRobot.multiply(r / 4.0f);

        // ROBOT TO WORLD TRANSFORMATION MATRIX INITIALIZATION
        robotToWorld.put(0, 0,  1.0f); robotToWorld.put(0, 1,  0.0f); robotToWorld.put(0, 2,  0.0f); robotToWorld.put(0, 3,  0.0f);
        robotToWorld.put(1, 0,  0.0f); robotToWorld.put(1, 1,  1.0f); robotToWorld.put(1, 2,  0.0f); robotToWorld.put(1, 3,  0.0f);
        robotToWorld.put(2, 0,  0.0f); robotToWorld.put(2, 1,  0.0f); robotToWorld.put(2, 2,  1.0f); robotToWorld.put(2, 3,  0.0f);
        robotToWorld.put(3, 0,  0.0f); robotToWorld.put(3, 1,  0.0f); robotToWorld.put(3, 2,  0.0f); robotToWorld.put(3, 3,  1.0f);

        // WORLD TO ROBOT TRANSFORMATION MATRIX INITIALIZATION
        worldToRobot.put(0, 0,  1.0f); worldToRobot.put(0, 1,  0.0f); worldToRobot.put(0, 2,  0.0f); worldToRobot.put(0, 3,  0.0f);
        worldToRobot.put(1, 0,  0.0f); worldToRobot.put(1, 1,  1.0f); worldToRobot.put(1, 2,  0.0f); worldToRobot.put(1, 3,  0.0f);
        worldToRobot.put(2, 0,  0.0f); worldToRobot.put(2, 1,  0.0f); worldToRobot.put(2, 2,  1.0f); worldToRobot.put(2, 3,  0.0f);
        worldToRobot.put(3, 0,  0.0f); worldToRobot.put(3, 1,  0.0f); worldToRobot.put(3, 2,  0.0f); worldToRobot.put(3, 3,  1.0f);

        // ROBOT TO WHEEL TRANSFORMATION MATRIX INITIALIZATION
        float rtwa = a + b;

        robotToWheel.put(0, 0,  1.0f); robotToWheel.put(0, 1,  1.0f); robotToWheel.put(0, 2,  rtwa); robotToWheel.put(0, 3,  1.0f);
        robotToWheel.put(1, 0,  1.0f); robotToWheel.put(1, 1, -1.0f); robotToWheel.put(1, 2, -rtwa); robotToWheel.put(1, 3,  1.0f);
        robotToWheel.put(2, 0,  1.0f); robotToWheel.put(2, 1,  1.0f); robotToWheel.put(2, 2, -rtwa); robotToWheel.put(2, 3, -1.0f);
        robotToWheel.put(3, 0,  1.0f); robotToWheel.put(3, 1, -1.0f); robotToWheel.put(3, 2,  rtwa); robotToWheel.put(3, 3, -1.0f);

        robotToWheel.multiply(1.0f / r);

        // POSITIONS INITIALIZATION
        wheelSensePrevPos.put(0, frontRightDrive.getCurrentPosition());
        wheelSensePrevPos.put(1, frontLeftDrive.getCurrentPosition());
        wheelSensePrevPos.put(2, backLeftDrive.getCurrentPosition());
        wheelSensePrevPos.put(3, backRightDrive.getCurrentPosition());
    }

    @Override
    public void init_loop() {
    }

    ///////////////////////////////////////////////////////// PLAY METHODS /////////////////////////////////////////////////////////

    @Override
    public void start() {

    }

    @Override
    public void loop() {
        // ENCODER PREPROCESSING
        wheelSensePos.put(0, frontRightDrive.getCurrentPosition() * wheelSenseCoef.get(0));
        wheelSensePos.put(1, frontLeftDrive.getCurrentPosition()  * wheelSenseCoef.get(1));
        wheelSensePos.put(2, backLeftDrive.getCurrentPosition()   * wheelSenseCoef.get(2));
        wheelSensePos.put(3, backRightDrive.getCurrentPosition()  * wheelSenseCoef.get(3));

        wheelSenseDelta = wheelSensePos.subtracted(wheelSensePrevPos);

        wheelSensePrevPos.put(0, wheelSensePos.get(0));
        wheelSensePrevPos.put(1, wheelSensePos.get(1));
        wheelSensePrevPos.put(2, wheelSensePos.get(2));
        wheelSensePrevPos.put(3, wheelSensePos.get(3));

        float wheelSenseDeltaMax = Math.max(
                Math.max(
                        Math.abs(wheelSenseDelta.get(0)),
                        Math.abs(wheelSenseDelta.get(1))
                ),
                Math.max(
                        Math.abs(wheelSenseDelta.get(2)),
                        Math.abs(wheelSenseDelta.get(3))
                )
        );

        float wheelSenseDeltaDiv;
        if (wheelSenseDeltaMax != 0.0f) {
            wheelSenseDeltaDiv = 1.0f / wheelSenseDeltaMax;
        }
        else {
            wheelSenseDeltaDiv = 0.0f;
        }

        wheelSenseDeltaNorm = wheelSenseDelta.multiplied(wheelSenseDeltaDiv);

        for (int i = 0; i < 4; i++) {
            float wheelPowerNormI      = wheelPowerNorm.get(i);
            float wheelSenseDeltaNormI = wheelSenseDeltaNorm.get(i);
            if ((wheelPowerNormI != 0.0f) && (wheelSenseDeltaNormI != 0.0f)) {
                float wheelScaleFactorI = wheelPowerNormI / wheelSenseDeltaNormI;
                wheelScaleFactor.put(i, wheelScaleFactorI);
            }
            else {
                wheelScaleFactor.put(i, 1.0f);
            }
        }

        // FORWARD KINEMATICS
        robotSenseDelta = wheelToRobot.multiplied(wheelSenseDelta);
        deviation += robotSenseDelta.get(3);

        // ODOMETRY
        robotToWorld.put(0, 0,  cosHeading); robotToWorld.put(0, 1, -sinHeading);
        robotToWorld.put(1, 0,  sinHeading); robotToWorld.put(1, 1,  cosHeading);

        worldSenseDelta = robotToWorld.multiplied(robotSenseDelta);

        /*float buffer = worldSenseDelta.get(0);
        worldSenseDelta.put(0, worldSenseDelta.get(2));
        worldSenseDelta.put(2, buffer);

        buffer = worldSenseDelta.get(1);
        worldSenseDelta.put(1, worldSenseDelta.get(3));
        worldSenseDelta.put(3, buffer);*/

        // LOCALIZATION
        worldPosition.add(worldSenseDelta);
        worldPosition.put(2, worldPosition.get(2) % 6.28f);



        telemetry.addData("Angle", worldPosition.get(2));

        // PATH INTERPOLATION
        worldPlanDelta = worldTarget.subtracted(worldPosition);

        if (Math.pow(2, worldPlanDelta.get(0)) + Math.pow(2, worldPlanDelta.get(1)) < 0.001f ) {
            requestOpModeStop();
        }

        // POSE CONTROL
        cosHeading = (float)Math.cos(worldPosition.get(2));
        sinHeading = (float)Math.sin(worldPosition.get(2));

        worldToRobot.put(0, 0,  cosHeading); worldToRobot.put(0, 1,  sinHeading);
        worldToRobot.put(1, 0, -sinHeading); worldToRobot.put(1, 1,  cosHeading);

        robotPlanDelta = worldToRobot.multiplied(worldPlanDelta);

        // COUPLING CONTROL
        robotPlanDelta.put(3, -couplingFactor * deviation);

        // INVERSE KINEMATICS
        wheelPlanDelta = robotToWheel.multiplied(robotPlanDelta);

        float wheelPlanDeltaMax = Math.max(
                Math.max(
                        Math.abs(wheelPlanDelta.get(0)),
                        Math.abs(wheelPlanDelta.get(1))
                ),
                Math.max(
                        Math.abs(wheelPlanDelta.get(2)),
                        Math.abs(wheelPlanDelta.get(3)))
        );

        float wheelPlanDeltaDiv;
        if (wheelPlanDeltaMax != 0.0f) {
            wheelPlanDeltaDiv = 1.0f / wheelPlanDeltaMax;
        }
        else {
            wheelPlanDeltaDiv = 0.0f;
        }

        wheelPlanDeltaNorm = wheelPlanDelta.multiplied(wheelPlanDeltaDiv);

        /** WHEEL CONTROL
         * As each wheel motor has a unique characteristic, we must use a feedback "follower" control. The theory is the same as
         * for a voltage follower op-amp. The final power to be driven to the wheels is the "wheelPowerNorm" vector. It has to be
         * clamped to [-1.0, +1.0] beforehand.
         */
        for (int i = 0; i < 4; i++) {
            float wheelPowerI = wheelPlanDeltaNorm.get(i) * wheelScaleFactor.get(i);
            wheelPower.put(i, wheelPowerI);
        }

        float wheelPowerMax = Math.max(
                Math.max(
                        Math.abs(wheelPower.get(0)),
                        Math.abs(wheelPower.get(1))
                ),
                Math.max(
                        Math.abs(wheelPower.get(2)),
                        Math.abs(wheelPower.get(3))
                ));

        float wheelPowerDiv;
        if (wheelPowerMax != 0.0f) {
            wheelPowerDiv = 1.0f / wheelPowerMax;
        }
        else {
            wheelPowerDiv = 0.0f;
        }

        wheelPowerNorm = wheelPower.multiplied(wheelPowerDiv);

        double frontRightPower  = Range.clip((double) wheelPowerNorm.get(0), -1.0d, 1.0d);
        double frontLeftPower = Range.clip((double) wheelPowerNorm.get(1), -1.0d, 1.0d);
        double backLeftPower   = Range.clip((double) wheelPowerNorm.get(2), -1.0d, 1.0d);
        double backRightPower  = Range.clip((double) wheelPowerNorm.get(3), -1.0d, 1.0d);

        frontLeftDrive.setPower(frontLeftPower);
        frontRightDrive.setPower(frontRightPower);
        backLeftDrive.setPower(backLeftPower);
        backRightDrive.setPower(backRightPower);

        telemetry.addData("position ", worldPosition.toString());
        telemetry.addData("target   ", worldTarget.toString());

        telemetry.addData("wheelSenseDelta ", wheelSenseDelta.toString());
        telemetry.addData("robotSenseDelta ", robotSenseDelta.toString());
        telemetry.addData("worldSenseDelta ", worldSenseDelta.toString());
        telemetry.addData("worldPlanDelta ", worldPlanDelta.toString());
        telemetry.addData("robotPlanDelta ", robotPlanDelta.toString());
        telemetry.addData("wheelPlanDelta ", wheelPlanDelta.toString());
    }

    ///////////////////////////////////////////////////////// STOP METHODS /////////////////////////////////////////////////////////

    @Override
    public void stop() {
        frontLeftDrive.setPower(0.0f);
        frontRightDrive.setPower(0.0f);
        backLeftDrive.setPower(0.0f);
        backRightDrive.setPower(0.0f);

        telemetry.addData("position ", worldPosition.toString());
        telemetry.addData("target   ", worldTarget.toString());

    }
}
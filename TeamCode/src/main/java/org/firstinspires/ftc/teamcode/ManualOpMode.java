package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.util.ElapsedTime;

import com.qualcomm.robotcore.util.Range;

@TeleOp(name="Manual OpMode")
public class ManualOpMode extends OpMode
{
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////

    private double turnSensitivity = 2; // Increasing sensitivity gives more priority to turning
    private double frontSensitivity = 1;
    private double sidewaysSensitivity = 1;

    // Initialize motors
    private DcMotor frontLeftDrive = null;
    private DcMotor frontRightDrive = null;
    private DcMotor backLeftDrive = null;
    private DcMotor backRightDrive = null;

    private double frontLeftPower = 0;
    private double frontRightPower = 0;
    private double backLeftPower = 0;
    private double backRightPower = 0;

    //private long loopCount = 0;
    // Random comment

    // Elapsed game time tracking.
    private ElapsedTime runtime = new ElapsedTime();
    private Movement movement;

    private double dpadSensitivity = 0.3; // How much the robot moves when dpad is used

    // Variables storing last state of gamepad buttons
    private boolean lastA = false;
    private boolean lastB = false;
    private boolean lastX = false;
    private boolean lastDescent = false;
    private boolean lastY = false;
    private double lastRuntime = 0.0;
    private double lastBPress = 0.0;
    private double lastXPress = 0.0;

    private boolean isOut = false;

    //private boolean lastBumperLeft = false;
    //private boolean lastBumperRight = false;

    // Variables storing state of grip and rotate
    //private boolean gripOpen = true;
    //private double rotationPos = 0;

    private double FLfirstTicks;
    private double BLfirstTicks;
    private double FRfirstTicks;
    private double BRfirstTicks;

    private double dpadTime;

    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    @Override
    public void init() {
        frontLeftDrive = hardwareMap.get(DcMotor.class, "front_left_drive");
        frontRightDrive = hardwareMap.get(DcMotor.class, "front_right_drive");
        backLeftDrive = hardwareMap.get(DcMotor.class, "back_left_drive");
        backRightDrive = hardwareMap.get(DcMotor.class, "back_right_drive");

        frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive.setDirection(DcMotor.Direction.FORWARD);
        backLeftDrive.setDirection(DcMotor.Direction.REVERSE);
        backRightDrive.setDirection(DcMotor.Direction.REVERSE);

        movement = new Movement(
                telemetry,
                hardwareMap.get(DcMotor.class, "front_left_drive"),
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive")
        );

        telemetry.addData("Movement & Pince", "Initialized");
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }
    ///////////////////////////////////////////////////////// MAIN LOOP /////////////////////////////////////////////////////////
    @Override
    public void loop() {
        movement.reset(true);
        movement.joystickTranslate(gamepad1.left_stick_y, gamepad1.left_stick_x);
        if (!(gamepad1.dpad_up || gamepad1.dpad_down || gamepad1.dpad_left || gamepad1.dpad_right)) {
            dpadTime = runtime.time();
        }
        movement.dpadTranslate(gamepad1.dpad_left, gamepad1.dpad_right, gamepad1.dpad_up, gamepad1.dpad_down, runtime.time() - dpadTime);

        telemetry.addData("Gamepad", "X (%.2f), Y (%.2f)", gamepad1.left_stick_x, gamepad1.left_stick_y);

        movement.bumperTurn(gamepad1.left_bumper, gamepad1.right_bumper, gamepad1.left_trigger, gamepad1.right_trigger);

        movement.apply();

        lastRuntime = runtime.time();

        telemetry.addData("Gamepad 1 Y", gamepad1.right_stick_y);

    }

    ///////////////////////////////////////////////////////// STOP METHOD /////////////////////////////////////////////////////////
    @Override
    public void stop() {
    }
}

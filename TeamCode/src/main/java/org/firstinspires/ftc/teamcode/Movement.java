/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class Movement
{
    private double turnSensitivity = 2; // Increasing sensitivity gives more priority to turning
    private double frontSensitivity = 1;
    private double sidewaysSensitivity = 1;
    private float turn = 0f;

    private ElapsedTime runtime = new ElapsedTime();

    private double maxDpadSensitivity = 0.4; // How much the robot moves when dpad is used
    private double timeToDpadFront = 3;
    private double timeToDpadSideways = 2;
    private double dpadMarginFront = 0.2;
    private double dpadMarginSideways = 0.3;

    // Initialize motors
    private DcMotor frontLeftDrive, frontRightDrive, backLeftDrive, backRightDrive, ticksMotor;

    private double frontLeftPower = 0;
    private double frontRightPower = 0;
    private double backLeftPower = 0;
    private double backRightPower = 0;

    private Telemetry telemetry;

    private double firstTurnTicks;
    private boolean rotateLock;
    private double ticksAfterFullRotation = 500; // TODO: Measure this with
    private double turnAngle, turnSpeed; // Angle in degrees

    Movement(Telemetry globalTelemetry, DcMotor FL, DcMotor FR, DcMotor BL, DcMotor BR) {

        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;

        // Assign motors from ManualOpMode
        frontLeftDrive  = FL; frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive = FR; frontRightDrive.setDirection(DcMotor.Direction.REVERSE);
        backLeftDrive  = BL;  backLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        backRightDrive = BR;  backRightDrive.setDirection(DcMotor.Direction.REVERSE);

        ticksMotor = backLeftDrive;
    }

    public void rotate(double angleInDegrees, double speed) {
        rotateLock = true;
        firstTurnTicks = ticksMotor.getCurrentPosition();
        turnAngle = angleInDegrees; turnSpeed = speed;
    }

    public void keepRotating() {
        if (turnAngle > 0) {
            if (firstTurnTicks + turnAngle / 360 * ticksAfterFullRotation > ticksMotor.getCurrentPosition()) {
                move(0, 0, turnSpeed);
            } else
                rotateLock = false;
        } else {
            if (firstTurnTicks - turnAngle / 360 * ticksAfterFullRotation < ticksMotor.getCurrentPosition()) {
                move(0, 0, -turnSpeed);
            } else
                rotateLock = false;
        }
    }

    public boolean checkRotating() {
        return rotateLock;
    }

    public void move(double front, double sideways, double turn) {
        backLeftPower  += sideways +front -turn;
        backRightPower += sideways -front +turn;
        /*backLeftPower*/ frontRightPower  += sideways +front +turn;
        /*backRightPower*/ frontLeftPower  += sideways -front -turn;
    }

    public void joystickTranslate(double front, double sideways) {
        /*if (Math.abs(sideways) < 0.1) {
            sideways = 0;
        } else if (Math.abs(sideways) > 0.1 || Math.abs(sideways) < 0.7) {
            sideways = 0.83*sideways+0.02;
        } else if (Math.abs(sideways) >= 0.7 || Math.abs(sideways) < 0.9) {
            sideways = 2*sideways-0.8;
        } else if (Math.abs(sideways) >=0.9) {
            sideways = 1;
        } else {
            sideways = 0;
        }

        if (Math.abs(front) < 0.1) {
            front = 0;
        } else if (Math.abs(front) > 0.1 || Math.abs(front) < 0.7) {
            front = 0.83*front+0.02;
        } else if (Math.abs(front) >= 0.7 || Math.abs(front) < 0.9) {
            front = 2*front-0.8;
        } else if (Math.abs(front) >=0.9) {
            front = 1;
        } else {
            front = 0;
        }*/
        move(
                sideways, // Exponentially increase controls
                front,
                0
        );
    }

    public void dpadTranslate(boolean left, boolean right, boolean up, boolean down, double time) {
        double sidewaysTime = time/timeToDpadSideways;
        double frontTime = time/timeToDpadFront;
        sidewaysTime += dpadMarginSideways;
        frontTime += dpadMarginFront;
        if (sidewaysTime > maxDpadSensitivity) sidewaysTime = maxDpadSensitivity;
        if (frontTime > maxDpadSensitivity) frontTime = maxDpadSensitivity;
        move(
                (right? sidewaysTime :0) - (left? sidewaysTime :0),
                (down? frontTime :0) - (up? frontTime :0),
                0
        );
    }

    public void bumperTurn(boolean leftBumper, boolean rightBumper, float leftTrigger, float rightTrigger) {
        if (leftBumper || rightBumper) {
            if (rightBumper)
                turn += 0.3;
            if (leftBumper)
                turn -= 0.3;
        } else {
            turn += rightTrigger;
            turn -= leftTrigger;
        }
        if (turn != 0) {
            reset(false); // Because Jeremy wanted to
        }
        move(0, 0, turn);
    }

    public void reset(boolean includeTurn) {
        frontLeftPower = 0;
        frontRightPower = 0;
        backLeftPower = 0;
        backRightPower = 0;
        if (includeTurn)
            turn = 0f;
    }

    public void apply() {
        frontLeftDrive.setPower(Range.clip(frontLeftPower, -1.0, 1.0));
        frontRightDrive.setPower(Range.clip(frontRightPower, -1.0, 1.0));
        backLeftDrive.setPower(Range.clip(backLeftPower, -1.0, 1.0));
        backRightDrive.setPower(Range.clip(backRightPower, -1.0, 1.0));

        telemetry.addData("Front", "left (%.2f), right (%.2f)", frontLeftPower, frontRightPower);
        telemetry.addData("Right", "left (%.2f), right (%.2f)", backLeftPower, backRightPower);
    }

    public void polarMove(double angle, double speed, double turn) {
        move(
                Math.cos(angle) * speed,
                Math.sin(angle) * speed,
                turn
        );
    }
}

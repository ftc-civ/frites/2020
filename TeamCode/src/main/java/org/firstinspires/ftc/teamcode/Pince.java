/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import android.util.Log;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import com.qualcomm.robotcore.util.ElapsedTime;

public class Pince
{
    private Telemetry telemetry;

    private DcMotor lift1;
    private DcMotor lift2;

    private CRServo arm;
    private Servo rotation;
    private Servo grip;

    private Servo foundation1;
    private Servo foundation2;

    private boolean gripOpen;
    private double gripOpenValue = 0.6;
    private double gripClosedValue = 0.35;

    private double foundation1OpenValue = 0.05;
    private double foundation1ClosedValue = 0.45;
    private double foundation2OpenValue = 0.85;
    private double foundation2ClosedValue = 0.45;


    private double moveBarPos = 0.47;
    private double changePos = 0.80;
    private double moveOutPos = gripOpenValue;
    private double insidePos = 0.47;

    private double upSensitivity = 1;
    private double outSensitivity = 1;
    private double turnSensitivity = 0.05;

    private double forwardsPos = 0.26;
    private double sidewaysPos = 0.62;
    private double backwardPos = 1.0;

    private double liftBlockingPower = -0.7;

    private boolean descending = false;

    private int lastLift1 = 0;
    private int lastLift2 = 0;

    private ElapsedTime runtime = new ElapsedTime();

    private int currentTurn = 0;
    private boolean foundationOpen = true;

    private double lastUp = 0;

    private double initialLift1 = 0;
    private double initialLift2 = 0;

    private boolean overrideDescent = false;

    /*
     * Constructor: Prepare motors
     */
    Pince(Telemetry globalTelemetry, DcMotor lift1Cons, DcMotor lift2Cons, CRServo armCons, Servo rotationCons, Servo gripCons, Servo foundationCons1, Servo foundationCons2) {

        ////////// TELEMETRY
        telemetry = globalTelemetry;

        ////////// MOTORS
        lift1 = lift1Cons;
        lift2 = lift2Cons;
        arm = armCons;
        rotation = rotationCons;
        grip = gripCons;
        foundation1 = foundationCons1;
        foundation2 = foundationCons2;

        lift1.setDirection(DcMotorSimple.Direction.FORWARD);
        lift2.setDirection(DcMotorSimple.Direction.REVERSE);

        // Tell the driver that initialization is complete.
        telemetry.addData("Pince", "Initialized");

        runtime.reset();

        lastLift1 = lift1.getCurrentPosition();
        lastLift2 = lift2.getCurrentPosition();

        initialLift1 = lift1.getCurrentPosition();
        initialLift2 = lift2.getCurrentPosition();
    }

    /*
       Opens or closes pince and returns its state
     */
    public void toggleState () {
        gripOpen = !gripOpen;
        if (gripOpen) {
            grip.setPosition(gripOpenValue);
        } else {
            grip.setPosition(gripClosedValue);
        }
    }

    public void checkLift(boolean still) {
        telemetry.addData("Lift 1: ", lift1.getCurrentPosition());
        telemetry.addData("Lift 2: ", lift2.getCurrentPosition());
        telemetry.addData("LastLift1", lastLift1);
        telemetry.addData("LastLift2", lastLift2);
        telemetry.addData("Descend", descending);
        if (still && lastLift1 < lift1.getCurrentPosition() && !descending) {
            lift1.setPower(liftBlockingPower);
            telemetry.addData("Blockinq lift1", "");
            lift2.setPower(liftBlockingPower);
            telemetry.addData("Blockinq lift2", "");
        }

        if (descending) {
            if (lift1.getCurrentPosition() < initialLift1) {
                lift1.setPower(1);
                lastLift1 = lift1.getCurrentPosition();
                lift1.setPower(1);
                lastLift2 = lift2.getCurrentPosition();
            } else {
                descending = false;
            }
        }
        telemetry.update();

    }

    public void open() {
        gripOpen = true;
        grip.setPosition(gripOpenValue);
    }

    public void close() {
        gripOpen = false;
        grip.setPosition(gripClosedValue);
    }

    public boolean isOpen() {
        return gripOpen;
    }

    public void move(double up, double out) {
        //if (lift1.getCurrentPosition() < 0 || up < 0 || overrideDescent) {
            lift1.setPower(up);
        //}
        //if (lift1.getCurrentPosition() < 0 || up < 0 || overrideDescent) {
            lift2.setPower(up);
        //}
        arm.setPower(out);
        if (up != 0) {
            lastLift1 = lift1.getCurrentPosition();
            lastLift2 = lift2.getCurrentPosition();
        }
    }

    public void overrideDescent() {
        overrideDescent = true;
    }

    public void joystickMove (double up, double out) {
        move(
                up * Math.abs(up) * upSensitivity,
                out * Math.abs(out) * outSensitivity
        );
    }

    public void toggleDescent() {
        descending = !descending;
    }

    public void turnForwards() {
        if (currentTurn == 0) {
            moveOut(false);
        } else {
            rotation.setPosition(forwardsPos);
            currentTurn = 2;
        }
    }

    void turnForwardsIgnore() {
        rotation.setPosition(forwardsPos);
        currentTurn = 2;
    }

    public void turnSideways() {
        if (currentTurn == 0) {
            moveOut(true);
        } else {
            rotation.setPosition(sidewaysPos);
            currentTurn = 1;
        }
    }

    public void delay(double seconds) {
        double startTime = runtime.time();
        while ((startTime + seconds) > runtime.time());
    }

    public int getPosition() {
        return currentTurn;
    }


    public void switchBetweenPositions() {
        if (currentTurn == 0 || currentTurn == 2)
            turnSideways();
        else
            turnForwards();
    }

    public void moveOut(boolean sideways) {
        grip.setPosition(moveBarPos);
        delay(0.1);
        rotation.setPosition(changePos);
        delay(0.1);
        grip.setPosition(moveOutPos);
        delay(0.1);
        if (sideways) {
            rotation.setPosition(sidewaysPos);
            currentTurn = 1;
        } else {
            rotation.setPosition(forwardsPos);
            currentTurn = 2;
        }
        delay(0.1);
    }

    public void turnBackwards() {
        if (currentTurn == 2) {
            turnSideways();
            delay(0.2);
        }
        grip.setPosition(moveOutPos);
        delay(0.1);
        rotation.setPosition(changePos);
        delay(0.3);
        grip.setPosition(moveBarPos);
        delay(0.1);
        rotation.setPosition(backwardPos);
        delay(0.1);
        currentTurn = 0;
        grip.setPosition(insidePos);
    }

    public void openFoundation() {
        foundation1.setPosition(foundation1OpenValue);
        foundation2.setPosition(foundation2OpenValue);

        foundationOpen = true;
    }

    public void closeFoundation() {
        foundation1.setPosition(foundation1ClosedValue);
        foundation2.setPosition(foundation2ClosedValue);
        foundationOpen = false;
    }

    public void toggleFoundation() {
        if (foundationOpen) {
            closeFoundation();
        } else {
            openFoundation();
        }

    }

}

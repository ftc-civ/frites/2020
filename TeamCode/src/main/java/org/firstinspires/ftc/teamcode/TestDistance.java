package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DistanceSensor;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

import java.util.Locale;

@TeleOp(name="TEST: Distance")
public class TestDistance extends OpMode
{
    private DistanceSensor distance;

    @Override
    public void init() {
        distance =  hardwareMap.get(DistanceSensor.class, "front_brick_sensor");
    }

    @Override
    public void init_loop() {
        telemetry.addData("Distance", String.format(Locale.US, "%.02f", distance.getDistance(DistanceUnit.CM)));
    }

    @Override
    public void start() {
    }

    @Override
    public void loop() {
        telemetry.addData("Distance", String.format(Locale.US, "%.02f", distance.getDistance(DistanceUnit.CM)));
    }

    @Override
    public void stop() {
    }
}

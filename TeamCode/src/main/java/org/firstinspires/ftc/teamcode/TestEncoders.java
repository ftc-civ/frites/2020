package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

@TeleOp(name="TEST: Motor Encoders")
public class TestEncoders extends OpMode
{
    private DcMotor frontLeftDrive, frontRightDrive, backLeftDrive, backRightDrive;
    private double FLfirstTicks, FRfirstTricks, BLfirstTicks, BRfirstTicks;

    public void init_ticks() {
        FLfirstTicks = frontLeftDrive.getCurrentPosition();
        FRfirstTricks = frontRightDrive.getCurrentPosition();
        BLfirstTicks = backLeftDrive.getCurrentPosition();
        BRfirstTicks = backRightDrive.getCurrentPosition();
    }

    public void logTicks() {
        telemetry.addData("FL", FLfirstTicks - frontLeftDrive.getCurrentPosition());
        telemetry.addData("FR", FRfirstTricks - frontRightDrive.getCurrentPosition());
        telemetry.addData("BL", BLfirstTicks - backLeftDrive.getCurrentPosition());
        telemetry.addData("BR", BRfirstTicks - backRightDrive.getCurrentPosition());
    }

    @Override
    public void init() {
        frontLeftDrive = hardwareMap.get(DcMotor.class, "front_left_drive"); frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive = hardwareMap.get(DcMotor.class, "front_right_drive"); frontRightDrive.setDirection(DcMotor.Direction.FORWARD);
        backLeftDrive = hardwareMap.get(DcMotor.class, "back_left_drive"); backLeftDrive.setDirection(DcMotor.Direction.REVERSE);
        backRightDrive = hardwareMap.get(DcMotor.class, "back_right_drive"); backRightDrive.setDirection(DcMotor.Direction.REVERSE);
        init_ticks();
    }

    @Override
    public void init_loop() { logTicks(); }

    @Override
    public void start() { init_ticks(); }

    @Override
    public void loop() { logTicks(); }

    @Override
    public void stop() {}
}

package org.firstinspires.ftc.teamcode;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.CameraName;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;

import java.util.List;
import java.util.Locale;

@Autonomous(name="Vuforia Auto 2")
public class VuforiaAutoOpMode extends LinearOpMode
{
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////
    private double turnSensitivity = 2; // Increasing sensitivity gives more priority to turning
    private double frontSensitivity = 1;
    private double sidewaysSensitivity = 1;

    // Initialize motors
    private DcMotor frontLeftDrive = null;
    private DcMotor frontRightDrive = null;
    private DcMotor backLeftDrive = null;
    private DcMotor backRightDrive = null;

    private double frontLeftPower = 0;
    private double frontRightPower = 0;
    private double backLeftPower = 0;
    private double backRightPower = 0;

    private float hsvValues[] = {0F, 0F, 0F};
    private final double SCALE_FACTOR = 255;

    //private long loopCount = 0;

    //private MatrixF field = new GeneralMatrixF(36, 36);


    // Elapsed game time tracking.
    private ElapsedTime runtime = new ElapsedTime();
    private Pince pince;
    private Movement movement;

    private double dpadSensitivity = 0.3; // How much the robot moves when dpad is used

    // Variables storing last state of gamepad buttons
    private boolean lastA = false;
    private boolean lastB = false;
    private boolean lastX = false;
    private boolean lastDescent = false;
    private boolean lastY = false;
    private double lastRuntime = 0.0;
    private double lastBPress = 0.0;
    private double lastXPress = 0.0;

    private boolean isOut = false;

    //private boolean lastBumperLeft = false;
    //private boolean lastBumperRight = false;

    // Variables storing state of grip and rotate
    //private boolean gripOpen = true;
    //private double rotationPos = 0;

    private boolean hasBrick = false;

    private ColorSensor frontBrickColorSensor;
    private DistanceSensor frontBrickDistanceSensor;

    private StoneDetector stoneDetector;
    private VuforiaNavigator vuforiaNavigator;

    private enum State { GETSKYSTONE, GOTOBUILDING, GOTOLOADING, MOVEFOUNDATION, PUTONFOUNDATION, PUTBESIDE, PARKLINE };
    private State state = State.GOTOLOADING;

    private enum Side { BLUE, RED };
    private Side side = Side.BLUE;

    private VectorF translation;
    private VectorF orientation;

    private VuforiaLocalizer vuforia;
    private VuforiaLocalizer.Parameters parameters;

    private static final String VUFORIA_KEY =
            "AeR4Ufv/////AAABmSp+W7zrm0pWhA6GMhExjldWqRUlKMRyJSF9/NFxur3/ODgvpBoN9r5lsuZcdL14xnyvEtHRI5rgNAjpLrP4WK8XIMPsfpGpWKz3sk2tqcCZbNB2Z4Pgl/lqjvsEugzW3pwyJKVvLw1ndgrPSKjGjDySNf+aeVSnpenWRMBEKxRlqrTpzxtPyM/sUz0o+JJpxpWO2PUARj6Vtzifg1dqjz+07hMR5PjJMVtS8dPi7jc8INS/m4JSefls+PtybcpjhvulYTRPfOgZIEx9kqVpq4wUH8Yx+QIvKwkieyJq581KluslmbtO705kmuE5WCfoHLw/ugG5XCGYM44nLXsQjjBXEhFhVkkFgMsMwNH+EPKq";

    public void initVuforia(CameraName webcam1) {
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);
        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraName = webcam1;
        vuforia = ClassFactory.getInstance().createVuforia(parameters);
    }


    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    public void initOp() {
        translation = new VectorF(0, 0, 0);
        frontLeftDrive = hardwareMap.get(DcMotor.class, "front_left_drive");
        frontRightDrive = hardwareMap.get(DcMotor.class, "front_right_drive");
        backLeftDrive = hardwareMap.get(DcMotor.class, "back_left_drive");
        backRightDrive = hardwareMap.get(DcMotor.class, "back_right_drive");

        frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive.setDirection(DcMotor.Direction.FORWARD);
        backLeftDrive.setDirection(DcMotor.Direction.REVERSE);
        backRightDrive.setDirection(DcMotor.Direction.REVERSE);

        pince = new Pince(
                telemetry,
                hardwareMap.get(DcMotor.class, "lift_motor1"),
                hardwareMap.get(DcMotor.class, "lift_motor2"),
                hardwareMap.get(CRServo.class, "arm_servo"),
                hardwareMap.get(Servo.class, "rotation_servo"),
                hardwareMap.get(Servo.class, "grip_servo"),
                hardwareMap.get(Servo.class, "foundation_servo1"),
                hardwareMap.get(Servo.class, "foundation_servo2")
        );

        movement = new Movement(
                telemetry,
                hardwareMap.get(DcMotor.class, "front_left_drive"),
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive")
        );

        frontBrickColorSensor = hardwareMap.get(ColorSensor.class, "front_brick_sensor");
        frontBrickDistanceSensor = hardwareMap.get(DistanceSensor.class, "front_brick_sensor");

        telemetry.addData("Pince", "Opening");
        telemetry.update();
        pince.open();

        telemetry.addData("VuforiaOpMode", "Initialized");

        WebcamName webcam = hardwareMap.get(WebcamName.class, "Webcam 1");

        initVuforia(webcam);

        int tfodMon = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());

        stoneDetector = new StoneDetector(telemetry, vuforia, tfodMon);
        vuforiaNavigator = new VuforiaNavigator(telemetry, vuforia, parameters);

        /*for (int i = 0; i < 36; i++) {
            for (int j = 0; j < 36; j++) {
                field.put(i, j, 0);
            }
        }

        for (int i = 17; i < 19; i++) {
            for (int j = 17; j < 18; j++) {
                field.put(i, j, 1);
            }
        }

        if (side == Side.BLUE) {
            for (int i = 0; i < 17; i++) {
                for (int j = 0; j < 36; j++) {
                    field.put(i, j, 1);
                }
            }
        } else {
            for (int i = 19; i < 36; i++) {
                for (int j = 0; j < 36; j++) {
                    field.put(i, j, 1);
                }
            }
        }*/
        telemetry.update();

    }

    private void goToPosition(VectorF positionTarget) {
        VectorF robotToPos = positionTarget.subtracted(translation);
        float front = robotToPos.get(0); // Todo: map to 0 - 1
        float sideways = robotToPos.get(1);
        float maxPow = Math.max(Math.abs(front), Math.abs(sideways));
        if (maxPow > 1) {
            front /= maxPow;
            sideways /= maxPow;
        }
        movement.move(front, sideways, 0);
        translation.put(0, translation.get(0) + front);
        translation.put(0, translation.get(1) + sideways);
    }

    /*@Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }*/
    ///////////////////////////////////////////////////////// MAIN LOOP /////////////////////////////////////////////////////////
    public void loopOp() {
        vuforiaNavigator.check();
        if (vuforiaNavigator.targetVisible) {
            translation = vuforiaNavigator.translation;
            orientation = vuforiaNavigator.translation;
        }
        telemetry.addData("Translation ",translation.toString());
        if (movement.checkRotating()) {
            movement.keepRotating();
            telemetry.addData("Status:", "Rotating Robot");
        } else {
            movement.reset(true);
            // Look for skystone with tensorflow and grab it
            if (state == State.GETSKYSTONE) {
                List<Recognition> updatedRecognitions = stoneDetector.checkForStones();
                if (updatedRecognitions != null) {
                    Recognition skystone = null;
                    for (Recognition recognition : updatedRecognitions) {
                    /*telemetry.addData(String.format("label (%d)", i), recognition.getLabel());
                    telemetry.addData(String.format("  left,top (%d)", i), "%.03f , %.03f",
                            recognition.getLeft(), recognition.getTop());
                    telemetry.addData(String.format("  right,bottom (%d)", i), "%.03f , %.03f",
                            recognition.getRight(), recognition.getBottom());*/
                        if (recognition.getLabel() == "Skystone") {
                            skystone = recognition;
                            break;
                        }
                    }
                    if (skystone != null) {
                        movement.move(skystone.getLeft() / 600, -0.5, 0);
                        telemetry.addData("PolarMove", skystone.getLeft() / 600);
                        Color.RGBToHSV(
                                (int) (frontBrickColorSensor.red() * SCALE_FACTOR),
                                (int) (frontBrickColorSensor.green() * SCALE_FACTOR),
                                (int) (frontBrickColorSensor.blue() * SCALE_FACTOR),
                                hsvValues
                        );
                        telemetry.addData("Distance: ", String.format(Locale.US, "%.02f", frontBrickDistanceSensor.getDistance(DistanceUnit.CM)));
                        telemetry.addData("Saturation : ", hsvValues[1]);
                        telemetry.addData("Brightness: ", hsvValues[2]);
                        if ((frontBrickDistanceSensor.getDistance(DistanceUnit.CM) > 10 || Double.isNaN(frontBrickDistanceSensor.getDistance(DistanceUnit.CM))) && hsvValues[2] > 80 && hsvValues[2] < 1400 && hsvValues[1] < 0.5) {
                            state = State.GOTOBUILDING;
                            pince.toggleFoundation();
                        }
                    } else {
                        movement.move(-0.5, 0, 0);
                    }
                    movement.apply();
                }

                // Go to loading zone
            } else if (state == State.GOTOLOADING) {
                VectorF target = new VectorF(30, 30, 0);

                goToPosition(target);
                // Go to building zone
            } else if (state == State.GOTOBUILDING) {

                // Drop a stone (not on foundation)
            } else if (state == State.PUTBESIDE) {

                // Drop a stone on the foundation
            } else if (state == State.PUTONFOUNDATION) {

                // Move foundation to building site
            } else if (state == State.MOVEFOUNDATION) {

                // Move to line TODO: Check if we should use skybridge or position
            } else if (state == State.PARKLINE) {

            }
            telemetry.update();
        }

    }

    ///////////////////////////////////////////////////////// STOP METHOD /////////////////////////////////////////////////////////
    /*@Override
    public void stop() {
    }*/
    @Override
    public void runOpMode() {
        initOp();
        waitForStart();
        while(opModeIsActive()) {
            loopOp();
        }
    }
}
